import { Routes, RouterModule } from '@angular/router';

export const ROUTES: Routes = [
    {
        path: '',
        loadChildren: './components/main/main.module#MainModule'
      },
      {
        path: 'profile',
        loadChildren: './components/profile/profile.module#ProfileModule'
      },
      {path: '**', redirectTo: '/404'}
];
