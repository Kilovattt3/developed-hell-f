import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { SharedModule } from '../../shared/shared.module';

import { ContentPageModule } from './content-page/content-page.module';
import { MainComponent } from './main.component';


@NgModule({
  imports: [
    CommonModule,
    ContentPageModule,
    MainRoutingModule,
    SharedModule
  ],
  declarations: [MainComponent]
})
export class MainModule { }
