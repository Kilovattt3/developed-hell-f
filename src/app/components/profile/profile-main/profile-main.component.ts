import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatIconRegistry, MatTableDataSource, MatSort } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-profile-main',
  templateUrl: './profile-main.component.html',
  styleUrls: ['./profile-main.component.scss']
})
export class ProfileMainComponent implements OnInit, AfterViewInit {
  displayedColumns = ['number', 'uploadDate', 'updateDate', 'evaluation', 'name', 'episodes', 'status'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort) sort: MatSort;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'user-add',
    sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/user-add-black.svg'));
    iconRegistry.addSvgIcon(
      'message',
    sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/Message_digital.svg'));
    iconRegistry.addSvgIcon(
      'eye-blocked',
    sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/eye-blocked.svg'));
    iconRegistry.addSvgIcon(
      'envelope',
    sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/envelope.svg'));
    iconRegistry.addSvgIcon(
      'chart',
    sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/stats-pie-chart.svg'));
    iconRegistry.addSvgIcon(
      'tweaks',
    sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/equalizer.svg'));
    iconRegistry.addSvgIcon(
      'user',
    sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/user.svg'));
    iconRegistry.addSvgIcon(
      'users',
    sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/users.svg'));
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}


export interface Element {
  number: number;
  uploadDate: string;
  updateDate: string;
  evaluation: number;
  name: string;
  episodes: number;
  status: string;
}

const ELEMENT_DATA: Element[] = [
  {number: 1, uploadDate: '12.03.2018', updateDate: '12.03.2018', evaluation: 2, name: 'Name_1', episodes: 12, status:  'Preview'},
  {number: 2, uploadDate: '24.02.2018', updateDate: '12.03.2018', evaluation: 5, name: 'Name_2', episodes: 55, status: 'Did not show up'},
  {number: 3, uploadDate: '01.01.2017', updateDate: '12.03.2018', evaluation: 4, name: 'Name_3', episodes: 3, status: 'Released'},
];
