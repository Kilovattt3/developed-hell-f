import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePersonalComponent } from './profile-personal/profile-personal.component';
import { ProfileMainComponent } from './profile-main/profile-main.component';
import { ProfileSecurityComponent } from './profile-security/profile-security.component';
import {ProfilePasswordComponent} from './profile-password/profile-password.component';
import { ProfileComponent } from './profile.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ProfileMainComponent
  },
  {
    path: '',
    component: ProfileComponent,
    children: [
      {
        path: 'personal',
        pathMatch: 'full',
        component: ProfilePersonalComponent
      },
      {
        path: 'security',
        pathMatch: 'full',
        component: ProfileSecurityComponent
      },
      {
        path: 'password',
        pathMatch: 'full',
        component: ProfilePasswordComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
