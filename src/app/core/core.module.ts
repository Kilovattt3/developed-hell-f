import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { throwIfAlreadyLoaded } from './module-import-guard';

import { MaterialModule } from './material/material.module';

import { HeaderComponent } from './header/header.component';

import { LiveBackgroundService } from '../services/live-background.service';
import { SideBarModule } from './side-bar/side-bar.module';

@NgModule({
  imports: [
    CommonModule,
    SideBarModule,
    MaterialModule
  ],
  declarations: [
    HeaderComponent
  ],
  exports: [
    HeaderComponent,
    SideBarModule
  ],
  providers: [
    LiveBackgroundService
  ]
})
export class CoreModule {
  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
