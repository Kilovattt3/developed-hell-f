import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'menu',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/Text Align Justify.svg'));
    iconRegistry.addSvgIcon(
      'search',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/search.svg'));
    iconRegistry.addSvgIcon(
      'compass',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/Compass.svg'));
    iconRegistry.addSvgIcon(
      'attention',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/attention.svg'));
    iconRegistry.addSvgIcon(
      'home',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/home.svg'));
    iconRegistry.addSvgIcon(
      'chevronLeft',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/chevron-left.svg'));
  }
  user = true;

  modeIndex = 0;
  get mode() { return ['side', 'over', 'push'][this.modeIndex]; }

  openSide = false;

  handleClickOpen() {
    this.openSide = !this.openSide;
  }

  ngOnInit() {
  }

}
