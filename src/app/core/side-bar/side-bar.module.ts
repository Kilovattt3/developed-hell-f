import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatSidenavModule,
  MatIconModule
} from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../../shared/shared.module';

import { SideBarComponent } from './side-bar.component';

const UI_COMPONENTS = [
  SideBarComponent
];

@NgModule({
  imports: [
    CommonModule,
    MatSidenavModule,
    MatIconModule,
    FlexLayoutModule,
    SharedModule
  ],
  declarations: [UI_COMPONENTS],
  exports: UI_COMPONENTS,
})
export class SideBarModule { }
