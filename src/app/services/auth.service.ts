import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

import { StorageService } from './storage.service';
import { environment } from '../../environments/environment';

const apiHost = environment.api;

@Injectable()
export class AuthService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private storageService: StorageService
  ) {}

  register(user) {
    return this.http.post(`${apiHost}`, user)
      .pipe(
        catchError((error) => {
          return Observable.throw(error);
        })
      );
  }

  signIn(user) {
    return this.http.post(`${apiHost}`, user)
      .pipe(
        map(token => {
          this.storageService.set('token', (token as any));
          return true;
        }),
        catchError((error) => {
          return Observable.throw(error);
        })
      );
  }

  signOut() {
    this.storageService.clear('token');
    this.router.navigate(['']);
  }
}
