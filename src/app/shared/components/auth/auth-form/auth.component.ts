import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  loginForm: FormGroup = null;
  registerForm: FormGroup = null;

  selectedForm = false;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private fb: FormBuilder) {
    const pwdValidators: ValidatorFn[] = [Validators.required, Validators.minLength(6), Validators.maxLength(20)];

    iconRegistry.addSvgIcon(
      'email',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/envelope.svg'));
    iconRegistry.addSvgIcon(
      'pass',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/lock.svg'));
    iconRegistry.addSvgIcon(
      'gift1',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/gift1.svg'));
    iconRegistry.addSvgIcon(
      'user',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/person_mat.svg'));

    this.loginForm = fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    });

    this.registerForm = fb.group({
      nickname: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: fb.group({
        pwd: ['', pwdValidators],
        confirm: ['', pwdValidators]
      })
    });

  }

  ngOnInit() {
  }

  toggleForm() {
    this.selectedForm = !this.selectedForm;
  }

  onLoginSubmit() {
    console.log('onLoginSubmit');
  }

  onRegisterSubmit() {
    console.log('onRegisterSubmit');
  }

}
