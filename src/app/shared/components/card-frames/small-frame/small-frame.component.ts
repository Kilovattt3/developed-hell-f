import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-small-frame',
  templateUrl: './small-frame.component.html',
  styleUrls: ['./small-frame.component.scss']
})
export class SmallFrameComponent implements OnInit {

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'fire',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/Fire.svg'));
    iconRegistry.addSvgIcon(
      'stats-bars',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/stats-bars.svg'));
    iconRegistry.addSvgIcon(
      'eye',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/eye.svg'));
    iconRegistry.addSvgIcon(
      'message_digital',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/Message_digital.svg'));
  }

  ngOnInit() {
  }

}
