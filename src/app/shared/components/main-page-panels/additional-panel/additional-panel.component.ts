import { Component, OnInit, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

import * as tooltips from '../../../models/main-page-tooltips';

@Component({
  selector: 'app-additional-panel',
  templateUrl: './additional-panel.component.html',
  styleUrls: ['./additional-panel.component.scss']
})
export class AdditionalPanelComponent implements OnInit {
  @Input() formatType: string;
  tooltipType: string;

  panelType: any = {};

  animeFormats: any = {
    isExpanded: false,
    title: 'Формат аниме',
    content: [{
      title: 'TV',
      isEnabled: false,
      selector: '',
    }, {
      title: 'OVA',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Фильм',
      isEnabled: false,
      selector: '',
    }, {
      title: 'ONA',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Спешел',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Клип',
      isEnabled: false,
      selector: '',
    }],
  };

  filmFormats: any = {
    isExpanded: false,
    title: 'Формат фильма',
    content: [{
      title: 'Короткометражный фильм',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Полнометражный фильм',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Сериал',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Документальное кино',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Научно-популярный фильм',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Любительское кино',
      isEnabled: false,
      selector: '',
    }],
  };

  statuses: any = {
    isExpanded: false,
    title: 'Статус',
    content: [{
      title: 'Анонс',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Онгоинг',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Релиз',
      isEnabled: false,
      selector: '',
    }],
  };

  types: any = {
    isExpanded: false,
    title: 'Тип',
    content: [{
      title: 'Сиквел',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Приквел',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Интерквел',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Мидквел',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Пролог',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Ремейк',
      isEnabled: false,
      selector: '',
    }, {
      title: 'Спин-офф',
      isEnabled: false,
      selector: '',
    }]
  };

  ratings: any = {
    isExpanded: false,
    title: 'Рейтинг',
    content: [{
      title: 'G',
      isEnabled: false,
      selector: '',
    }, {
      title: 'PG',
      isEnabled: false,
      selector: '',
    }, {
      title: 'PG-13',
      isEnabled: false,
      selector: '',
    }, {
      title: 'R',
      isEnabled: false,
      selector: '',
    }, {
      title: 'NC-17',
      isEnabled: false,
      selector: '',
    }],
  };

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'plus',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/plus.svg'));
    iconRegistry.addSvgIcon(
      'minus',
    sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/minus.svg'));
    iconRegistry.addSvgIcon(
      'check-empty-',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/check-empty -.svg'));
    iconRegistry.addSvgIcon(
      'check-empty+',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/check-empty +.svg'));
  }

  ngOnInit() {
    switch (this.formatType) {
      case 'animeFormats':
        this.panelType = this.animeFormats;
        this.tooltipType = 'animeFormatsTooltips';
        break;
      case 'filmFormats':
        this.panelType = this.filmFormats;
        this.tooltipType = 'filmFormatsTooltips';
        break;
      case 'statuses':
        this.panelType = this.statuses;
        this.tooltipType = 'statusTooltips';
        break;
      case 'types':
        this.panelType = this.types;
        this.tooltipType = 'typeTooltips';
        break;
      case 'ratings':
        this.panelType = this.ratings;
        this.tooltipType = 'ratingTooltips';
        break;
    }
  }

  getTooltip(title: string): string {
    if (Object.keys(tooltips[this.tooltipType]).length === 0 && tooltips[this.tooltipType].constructor === Object) {
      return;
    }
    return tooltips[this.tooltipType][title];
  }

  changeSelector(panel: string, selector: string, index: number): void {
    let requiredPanel;

    if (panel === 'Формат аниме') {
      requiredPanel = this.animeFormats;
    } else if (panel === 'Формат фильма') {
      requiredPanel = this.filmFormats;
    } else if (panel === 'Статус') {
      requiredPanel = this.statuses;
    } else if (panel === 'Тип') {
      requiredPanel = this.types;
    } else if (panel === 'Рейтинг') {
      requiredPanel = this.ratings;
    }

    switch (selector) {
      case 'plus': {
        requiredPanel.content[index].selector = 'minus';
        break;
      }
      case 'minus': {
        requiredPanel.content[index].selector = '';
        break;
      }
      case '': {
        requiredPanel.content[index].selector = 'plus';
        break;
      }
    }
  }
}
