import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-main-panel',
  templateUrl: './main-panel.component.html',
  styleUrls: ['./main-panel.component.scss']
})
export class MainPanelComponent implements OnInit {
  @Output() toggleViewMode: EventEmitter<string> = new EventEmitter<string>();

  defaultViewMode: string;
  viewModes = [
    'large-mode',
    'small-mode',
    'list-mode',
  ];

  defaultSortMode: string;
  sortModes = [
    'sort-alpha-asc',
    'sort-amount-desc',
    'sort-move-down',
    'sort-numberic-desc',
  ];

  panel: any = {
    isExpanded: true,
    title: 'Каталог',
  };

    constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
      this.defaultSortMode = 'sort-alpha-asc';
      this.defaultViewMode = 'large-mode';

    iconRegistry.addSvgIcon(
      'large-mode',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/th-large.svg'));
    iconRegistry.addSvgIcon(
      'small-mode',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/th.svg'));
    iconRegistry.addSvgIcon(
      'list-mode',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/th-list.svg'));


    iconRegistry.addSvgIcon(
      'sort-alpha-asc',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/sort-alpha-asc.svg'));
    iconRegistry.addSvgIcon(
      'sort-amount-desc',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/sort-amount-desc.svg'));
    iconRegistry.addSvgIcon(
      'sort-move-down',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/sort-move-down.svg'));
    iconRegistry.addSvgIcon(
      'sort-numberic-desc',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/sort-numberic-desc.svg'));
    iconRegistry.addSvgIcon(
      'plus',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/plus.svg'));
    iconRegistry.addSvgIcon(
      'minus',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/main/minus.svg'));
  }

  ngOnInit() {
  }

  toggleMode(mode: string): any {
    this.toggleViewMode.emit(mode);
  }

}
