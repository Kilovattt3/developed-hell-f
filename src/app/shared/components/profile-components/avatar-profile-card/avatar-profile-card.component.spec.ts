import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarProfileCardComponent } from './avatar-profile-card.component';

describe('AvatarProfileCardComponent', () => {
  let component: AvatarProfileCardComponent;
  let fixture: ComponentFixture<AvatarProfileCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvatarProfileCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarProfileCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
