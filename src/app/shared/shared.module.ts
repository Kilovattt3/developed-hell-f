import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { DragDropDirective } from './directives/drag-drop/drag-drop.directive';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularCropperjsModule } from 'angular-cropperjs';
import { GalleryModule } from '@ngx-gallery/core';
import { NvD3Module } from 'ng2-nvd3';
import 'd3';
import 'nvd3';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatIconModule,
  MatInputModule,
  MatFormFieldModule,
  MatListModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSidenavModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTooltipModule
} from '@angular/material';
import { LargeFrameComponent } from './components/card-frames/large-frame/large-frame.component';
import { SmallFrameComponent } from './components/card-frames/small-frame/small-frame.component';
import { ListFrameComponent } from './components/card-frames/list-frame/list-frame.component';
import { CatalogsPanelComponent } from './components/main-page-panels/catalogs-panel/catalogs-panel.component';
import { ProfileSidebarComponent } from './components/profile-components/profile-sidebar/profile-sidebar.component';
import { ProfileCardComponent } from './components/profile-components/profile-card/profile-card.component';
import { AvatarProfileCardComponent } from './components/profile-components/avatar-profile-card/avatar-profile-card.component';
import { AvatarComponent } from './components/profile-components/avatar/avatar.component';
import { ContentSidebarComponent } from './components/content-page/content-sidebar/content-sidebar.component';
import { D3BarChartComponent } from './components/d3/d3-bar-chart/d3-bar-chart.component';
import { ContentSliderComponent } from './components/content-page/content-slider/content-slider.component';
import { CoverSliderComponent } from './components/content-page/cover-slider/cover-slider.component';
import { D3BulletComponent } from './components/d3/d3-bullet/d3-bullet.component';
import { D3SunburstComponent } from './components/d3/d3-sunburst/d3-sunburst.component';
import { AuthComponent } from './components/auth/auth-form/auth.component';
import {InfoComponent} from './components/profile-components/info/info.component';
import { AdditionalPanelComponent } from './components/main-page-panels/additional-panel/additional-panel.component';
import { MainPanelComponent } from './components/main-page-panels/main-panel/main-panel.component';
import { SimpeInputComponent } from './components/ui/simpe-input/simpe-input.component';
import { ButtonComponent } from './components/ui/button/button.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    AngularCropperjsModule,
    GalleryModule.forRoot(),
    SweetAlert2Module.forRoot(),
    NvD3Module,
    InfiniteScrollModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatListModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule
  ],
  exports: [
    DragDropDirective,
    FlexLayoutModule,
    AngularCropperjsModule,
    InfiniteScrollModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatListModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    LargeFrameComponent,
    SmallFrameComponent,
    ListFrameComponent,
    CatalogsPanelComponent,
    AvatarComponent,
    ProfileSidebarComponent,
    AvatarProfileCardComponent,
    ProfileCardComponent,
    ContentSidebarComponent,
    D3BarChartComponent,
    ContentSliderComponent,
    CoverSliderComponent,
    D3BulletComponent,
    D3SunburstComponent,
    AuthComponent,
    AdditionalPanelComponent,
    MainPanelComponent,
    InfoComponent,
    SimpeInputComponent,
    ButtonComponent
  ],
  declarations: [
    DragDropDirective,
    LargeFrameComponent,
    SmallFrameComponent,
    ListFrameComponent,
    CatalogsPanelComponent,
    AvatarComponent,
    ProfileSidebarComponent,
    AvatarProfileCardComponent,
    ProfileCardComponent,
    ContentSidebarComponent,
    D3BarChartComponent,
    ContentSliderComponent,
    CoverSliderComponent,
    D3BulletComponent,
    D3SunburstComponent,
    AuthComponent,
    AdditionalPanelComponent,
    MainPanelComponent,
    InfoComponent,
    SimpeInputComponent,
    ButtonComponent
  ],
  providers: []
})
export class SharedModule { }
